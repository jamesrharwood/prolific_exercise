class CustomError(Exception):
    def __init__(self, message, status_code=None):
        self.status_code = status_code
        self.message = message

class BadRequestError(CustomError):
    def __init__(self, message='Bad request', status_code=400):
        self.status_code=status_code
        self.message=message

class DuplicateResponderError(BadRequestError):
    def __init__(self, message='This user has already completed this survey.', status_code=400):
        self.status_code = status_code
        self.message = message

class SurveyNotRecruitingError(BadRequestError):
    def __init__(self, message='This survey is no longer recruiting.', status_code=400):
        self.status_code=status_code
        self.message=message

class InvalidHashidError(BadRequestError):
    def __init__(self, message='ID not valid', status_code=400):
        self.status_code=status_code
        self.message=message

class UsernameNotUniqueError(BadRequestError):
    def __init__(self, message='Username not unique', status_code=400):
        self.status_code = status_code
        self.message = message

