from flask import jsonify
from werkzeug.http import HTTP_STATUS_CODES

from app.errors import bp
from app.errors.exceptions import BadRequestError
from schema import SchemaMissingKeyError, SchemaError

def api_error_response(status_code, message=None, type_=None):
    """send a json error response to a bad API request"""
    payload = {'error' : HTTP_STATUS_CODES.get(status_code, 'Unknown error')}
    if message:
        payload['message'] = message
    if type_:
        payload['type'] = type_
    response = jsonify(payload)
    response.status_code = status_code
    return response

@bp.app_errorhandler(BadRequestError)
def custom_error(error):
    return api_error_response(status_code=error.status_code, message=error.message, type_=error.__class__.__name__)

@bp.app_errorhandler(SchemaError)
def schema_error(error):
    return api_error_response(status_code=400, message=error.message, type_='RequestSchemaError')

@bp.app_errorhandler(SchemaMissingKeyError)
def schema_missing_key_error(error):
    return api_error_response(status_code=400, message=error.message, type_='RequestMissingKeyError')

@bp.app_errorhandler(404)
def NotFound(error):
    return api_error_response(status_code=404, message=error.message)
