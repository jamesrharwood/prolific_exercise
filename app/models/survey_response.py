from datetime import datetime

from app import db
from .hashid import HashidMixin

class SurveyResponse(db.Model, HashidMixin):
    __tablename__ = 'survey_responses'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    survey_id = db.Column(db.Integer, db.ForeignKey('surveys.id'))

    def __init__(self, user_id, survey_id):
        self.user_id = user_id
        self.survey_id = survey_id
        self.created_at = datetime.utcnow()

    def to_dict(self):
        return dict(
            id_ = self.get_hashid(),
            created_at = self.created_at.isoformat(),
            user_id = self.user.get_hashid(),
            survey_id = self.survey.get_hashid(),
        )

    def __repr__(self):
        return '<SurveyResponse: user_id: {}, survey_id: {}>'.format(self.user_id, self.survey_id)
