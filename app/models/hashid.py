import os
from hashids import Hashids
from flask import abort

from app.errors.exceptions import InvalidHashidError

hashid_maker = Hashids(salt=os.environ['HASHID_SALT'], min_length=24)

class NullQuery():
    def get(self, hashid):
        raise Exception("HashidMixin needs to be inherited to a DB model")
    def get_or_404(self, hashid):
        raise Exception("HashidMixin needs to be inherited to a DB model")

null_query = NullQuery()

class HashidMixin(object):
    id = None
    query = null_query #this gets overwritten when inherited by the database object

    def get_hashid(self):
        assert self.id, "No ID. Make sure HashidMixin has been inherited into a db object and that the object has been committed."
        return hashid_maker.encode(self.id)

    @classmethod
    def get(cls, hashid, raise_404=False):
        id_ = cls.read_hashid(hashid)
        item = cls.query.get(id_)
        if not item:
            if raise_404:
                abort(404)
            else:
                raise InvalidHashidError('Resource not found from ID. Check that you are using the correct ID is correct.')
        return item

    @classmethod
    def get_or_404(cls, hashid):
        return cls.get(hashid, raise_404=True)

    @classmethod
    def read_hashid(cls, hashid):
        if not hashid:
            raise InvalidHashidError('Please provide a {} ID.'.format(cls.__name__.lower()))
        if type(hashid) not in [str, unicode]:
            raise InvalidHashidError('{} ID should be a string.'.format(cls.__name__))
        id_ = hashid_maker.decode(hashid)
        if not id_:
            raise InvalidHashidError('{} ID string not valid.'.format(cls.__name__))
        return id_

