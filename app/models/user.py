from flask import url_for

from app import db
from .hashid import HashidMixin

class User(db.Model, HashidMixin):
    __tablename__='users'
    id = db.Column(db.Integer, primary_key=True)
    surveys = db.relationship('Survey', backref='user', lazy='dynamic')
    survey_repsonses = db.relationship('SurveyResponse', backref='user', lazy='dynamic')
    username = db.Column(db.String(64), index=True, unique=True)

    def to_dict(self):
        return dict(
            id_ = self.get_hashid(),
            username = self.username,
            surveys = url_for('api.get_user_surveys', user_id = self.get_hashid(), _external=True)
        )

    def __init__(self, username):
        self.username=username

    def __repr__(self):
        return '<User {}>'.format(self.username)

