from datetime import datetime
from flask import url_for

from app import db
from .hashid import HashidMixin
from .survey_response import SurveyResponse
from app.errors.exceptions import DuplicateResponderError, SurveyNotRecruitingError

class Survey(db.Model, HashidMixin):
    __tablename__='surveys'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, nullable=False)
    name = db.Column(db.String(128), nullable=False)
    available_places = db.Column(db.Integer, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    survey_responses = db.relationship('SurveyResponse', backref='survey', lazy='dynamic')

    def __init__(self, name, user_id, available_places=None):
        self.name = name
        self.user_id = user_id
        self.available_places = available_places
        self.created_at = datetime.utcnow()

    def create_survey_response(self, user_id):
        """Adds a new survey response to the survey, after checking that the survey is still recruiting and that the user has not already completed it.
        Param: userid:str
        Return: The SurveyResponse object added to the Survey"""
        if self.already_completed_by(user_id):
            raise DuplicateResponderError('This user has already completed this survey.'.format(user_id))
        if not self.still_recruiting:
            raise SurveyNotRecruitingError('Survey {} is not recruiting any more participants.'.format(self.get_hashid()))
        survey_response = SurveyResponse(user_id, self.id)
        return survey_response
    def already_completed_by(self, user_id):
        """checks whether a user has already completed the survey.
        Param: user_id
        Return: Bool"""
        query = self.survey_responses.filter_by(user_id=user_id)
        return bool(query.count())

    @property
    def still_recruiting(self):
        """Checks whether places are still available."""
        if self.available_places==None: #no limit was set
            return True
        elif self.survey_responses.count() < self.available_places:
            return True
        else:
            return False

    def to_dict(self):
        return dict(
            id_ = self.get_hashid(),
            name = self.name,
            available_places = self.available_places,
            user_id = self.user.get_hashid(),
            created_at = self.created_at.isoformat(),
            survey_responses = url_for('api.get_survey_responses', survey_id=self.get_hashid(), _external=True)
        )

    def __repr__(self):
        return '<Survey {}>.format(self.name)'
