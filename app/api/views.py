from schema import Schema, Optional
from flask import jsonify, request, url_for

from app import db
from app.errors.exceptions import UsernameNotUniqueError
from app.api import bp
from app.models import User, Survey, SurveyResponse

@bp.route('/users', methods=['POST'])
def create_user():
    schema = Schema({'username':basestring})
    data = schema.validate(request.get_json())
    username=data['username']
    existing_user = User.query.filter_by(username=username).first()
    if existing_user:
        raise UsernameNotUniqueError(message='Username already taken')
    user = User(username)
    db.session.add(user)
    db.session.commit()
    response = jsonify(user.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_user', user_id=user.get_hashid())
    return response

@bp.route('/users/<user_id>', methods=['GET'])
def get_user(user_id):
    user = User.get_or_404(user_id)
    return jsonify(user.to_dict())

@bp.route('/users/<user_id>/surveys', methods=['GET'])
def get_user_surveys(user_id):
    user = User.get_or_404(user_id)
    return jsonify([s.to_dict() for s in user.surveys])

@bp.route('/surveys', methods=['POST'])
def create_survey():
    schema = Schema({
        'user_id':basestring,
        'name':basestring,
        Optional('available_places'):int,
    })
    data = schema.validate(request.get_json())
    user = User.get(data['user_id']) #get the real user from the hashid
    places = data.get('available_places')
    survey = Survey(data['name'], user.id, places)
    db.session.add(survey)
    db.session.commit()
    db.session.refresh(survey)
    response = jsonify(survey.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_survey', survey_id=survey.get_hashid())
    return response

@bp.route('/surveys', methods=['GET'])
def get_surveys():
    return jsonify([s.to_dict() for s in Survey.query])

@bp.route('/surveys/<survey_id>', methods=['GET'])
def get_survey(survey_id):
    survey = Survey.get_or_404(survey_id)
    return jsonify(survey.to_dict())

@bp.route('/surveys/<survey_id>/responses', methods=['GET'])
def get_survey_responses(survey_id):
    survey = Survey.get_or_404(survey_id)
    return jsonify([r.to_dict() for r in survey.survey_responses])

@bp.route('/survey_responses', methods=['POST'])
def post_survey_response():
    schema = Schema({'user_id':basestring, 'survey_id':basestring})
    data = schema.validate(request.get_json())
    survey = Survey.get(data['survey_id'])
    user = User.get(data['user_id']) #user_id is a hashid, so we need to get the id from the real user object
    survey_response = survey.create_survey_response(user.id)
    db.session.add(survey_response)
    db.session.commit()
    response = jsonify(survey_response.to_dict())
    response.status_code=201
    response.headers['Location'] = url_for('api.get_survey_response', survey_response_id = survey_response.get_hashid())
    return response

@bp.route('survey_responses/<survey_response_id>', methods=['GET'])
def get_survey_response(survey_response_id):
    survey_response = SurveyResponse.get_or_404(survey_response_id)
    return jsonify(survey_response.to_dict())

