import unittest

from flask import url_for

from dotenv import load_dotenv
load_dotenv()

from app import create_app, db
from app.errors.exceptions import DuplicateResponderError, SurveyNotRecruitingError, InvalidHashidError
from app.models import User, Survey, SurveyResponse
from config import Config
from app import api

class TestConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite://' #Tests will write to memory so we have a fresh db each time

class BasicTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app(TestConfig)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.user = User("Username")
        self.ppt1 = User("Ppt1")
        self.ppt2 = User("Ppt2")
        self.ppt3 = User("Ppt3")
        db.session.add_all([self.user, self.ppt1, self.ppt2, self.ppt3])
        db.session.commit()
        self.survey = Survey("MySurvey", self.user.id)
        self.survey_limited = Survey('MyLimitedSurvey', self.user.id, 2)
        db.session.add_all([self.survey, self.survey_limited])
        db.session.commit()
        self.survey_response = self.survey_limited.create_survey_response(self.ppt1.id)
        db.session.add(self.survey_response)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_create_survey(self):
        self.assertTrue(self.survey)
        self.assertTrue(self.survey.created_at)
        self.assertTrue(self.survey.still_recruiting)

    def test_create_survey_with_limited_places(self):
        self.assertEqual(self.survey_limited.available_places, 2)

    def test_create_user(self):
        self.assertTrue(self.user)

    def test_create_survey_response(self):
        self.assertTrue(isinstance(self.survey_response, SurveyResponse))
        self.assertEqual(self.survey_response.user, self.ppt1)
        self.assertEqual(self.survey_response.survey, self.survey_limited)
        self.assertEqual(self.survey_limited.survey_responses.count(), 1)

    def test_survey_reaches_limit(self):
        'With a survey limited to 2 places, one place already filled, check that the 2nd ppt gets added and the 3rd raises an error.'
        self.assertEqual(self.survey_limited.available_places, 2)
        self.assertEqual(self.survey_limited.survey_responses.count(), 1)
        resp2 = self.survey_limited.create_survey_response(self.ppt2.id)
        db.session.add(resp2)
        db.session.commit()
        with self.assertRaises(SurveyNotRecruitingError):
            self.survey_limited.create_survey_response(self.ppt3.id)

    def test_dupliacte_responder(self):
        'Ppt1 tries to respond to a survey twice, gets an error'
        survey = self.survey_limited
        self.assertEqual(survey.survey_responses.first().user, self.ppt1)
        with self.assertRaises(DuplicateResponderError):
            survey.create_survey_response(self.ppt1.id)

    def test_survey_to_dict(self):
        survey_dict = self.survey.to_dict()
        self.assertTrue(isinstance(survey_dict['id_'], basestring))
        self.assertTrue(isinstance(survey_dict['user_id'], basestring))
        self.assertTrue(isinstance(survey_dict['created_at'], basestring))
        self.assertTrue(isinstance(survey_dict['name'], basestring))

    def test_survey_response_to_dict(self):
        response_dict = self.survey_response.to_dict()
        self.assertEqual(response_dict['id_'], self.survey_response.get_hashid())
        self.assertEqual(response_dict['user_id'], self.ppt1.get_hashid())
        self.assertEqual(response_dict['survey_id'], self.survey_limited.get_hashid())
        self.assertTrue(isinstance(response_dict['created_at'], basestring))

    def test_user_to_dict(self):
        user_dict = self.user.to_dict()
        self.assertEqual(user_dict['id_'], self.user.get_hashid())
        self.assertEqual(user_dict['surveys'], url_for('api.get_user_surveys', user_id=self.user.get_hashid(), _external=True))

    def test_invalid_hashid(self):
        with self.assertRaises(InvalidHashidError):
            Survey.get('abc')
        with self.assertRaises(InvalidHashidError):
            User.get(1)

    def test_api_get_user(self):
        response = api.views.get_user(self.user.get_hashid())
        self.assertEqual(response.status_code, 200)

    def test_api_get_survey(self):
        response = api.views.get_survey(self.survey.get_hashid())
        self.assertEqual(response.status_code, 200)

    def test_api_get_survey_response(self):
        response = api.views.get_survey_response(self.survey_response.get_hashid())
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()
