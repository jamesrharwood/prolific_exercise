import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    'Holds config settings'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False #turning this off stops sqlalchemy signalling every time a change is going to be made
    SERVER_NAME = 'localhost:5000'
