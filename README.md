#Prolific Developer Exercise

A simple API for the creation and retrieval of surveys, survey responses and users.

##Getting Started

###Prerequisites
You must have [Python 2.7](https://www.python.org/downloads/release/python-2715/) and [virtualenv](https://virtualenv.pypa.io/en/stable/installation/) installed.

###Installing
Clone this repository and cd into it

```
git clone git@bitbucket.org:jamesrharwood/prolific_exercise.git
cd prolific_exercise
```


Create environment variables in .flaskenv and .env

```
echo "FLASK_APP=app.py" >> .flaskenv
echo "FLASK_ENV=development" >> .flaskenv
echo "HASHID_SALT=hard_to_guess_salt" >> .env
```
NB use this HASHID_SALT so you can copy-paste the commands below in order without having to change the IDs.


Create and activate a virtual environment

```
virtualenv venv
source venv/bin/activate
```


Upgrade pip and install requirements

```
venv/bin/pip install --upgrade pip
venv/bin/pip install -r requirements.txt
```


Create the SQLite database

```
flask db upgrade
```



###Run tests

Check that the application is installed correctly

`venv/bin/python tests.py`


###Deploy the app locally

`flask run`



##API Usage

###API Versioning

The base endpoint for the API url is `http://127.0.0.1:5000/api/v1.0`, where `v1.0` specifies the API version so that future releases can have separate endpoints.

###Error Handling

The API uses conventional HTTP response codes to indicate success or failure of an API request. The status codes you will see include:

**200 - OK**    Everything worked as expected.

**201 - Created**    The request has been fulfilled and has resulted in a new resource being created.

**400 - Bad Request**    The request was unacceptable.

**404 - Not Found**    The requested resource does not exist


####Error Attributes

To give clarity to `400 - Bad Request` errors, these types of responses will include the following attributes:

`message` - a human readable explanation of what was wrong with the request.

`type` - a string code that describes the type of error. These may be:

- DuplicateResponderError: When creating a new survey response, this error is thrown when the participant has already completed that survey.
- SurveyNotRecruitingError: When creating a new survey response, this error is thrown when a survey's available places have already been filled.
- SchemaError: The POST payload included data in an incorrect format, e.g. the user supplied a string when an int was expected.
- SchemaMissingKeyError: The POST payload was missing a required piece of data.
- InvalidHashidError: The ID provided by the user could not be resolved into a valid resource identifier.


###Example Usage

For simplicity, example API calls are shown using [HTTPie](https://httpie.org/doc) instead of curl.

Run these commands within the virtual environment to use HTTPie installed from requirements.txt.

####Create a new user:

```
source venv/bin/activate
http POST http://localhost:5000/api/v1.0/users username=Username
```

returns

```
HTTP/1.0 201 CREATED
Content-Length: 70
Content-Type: application/json
Date: Mon, 06 Aug 2018 16:27:56 GMT
Location: http://localhost:5000/api/v1.0/users/YE3e2Oml7nxK5QGvrLjbWAw4
Server: Werkzeug/0.14.1 Python/2.7.10

{
    "id_": "YE3e2Oml7nxK5QGvrLjbWAw4",
    "surveys": "http://localhost:5000/api/v1.0/users/YE3e2Oml7nxK5QGvrLjbWAw4/surveys",
    "username": "Username"
}
```


####Create a new survey with 2 available places:

Specify `available_places` as an int.

`http POST http://localhost:5000/api/v1.0/surveys user_id=YE3e2Oml7nxK5QGvrLjbWAw4 name=MySurvey available_places:=2`

returns

```
HTTP/1.0 201 CREATED
...
{
    "available_places": 2,
    "created_at": "2018-08-06T16:28:50.407534",
    "id_": "YE3e2Oml7nxK5QGvrLjbWAw4",
    "name": "MySurvey",
    "survey_responses": "http://localhost:5000/api/v1.0/surveys/YE3e2Oml7nxK5QGvrLjbWAw4/responses",
    "user_id": "YE3e2Oml7nxK5QGvrLjbWAw4"
}
```


####Create some participants:

`http POST http://localhost:5000/api/v1.0/users username=Participant1`

`http POST http://localhost:5000/api/v1.0/users username=Participant2`

`http POST http://localhost:5000/api/v1.0/users username=Participant3`

returns

```
HTTP/1.0 201 CREATED
...
{
    "id_": "NeOdqrlb9YgeZx3X15o4DLMa",
     "surveys": "http://localhost:5000/api/v1.0/users/NeOdqrlb9YgeZx3X15o4DLMa/surveys",
     "username": "Participant1"
}
...
{
    "id_": "JW19n8Nj6rpzGQlq5RAOa4oL",
    "surveys": "http://localhost:5000/api/v1.0/users/JW19n8Nj6rpzGQlq5RAOa4oL/surveys",
    "username": "Participant2"
}
...
{
    "id_": "jqXZEdwvYrQn7gk6V0JP75RG",
    "surveys": "http://localhost:5000/api/v1.0/users/jqXZEdwvYrQn7gk6V0JP75RG/surveys",
    "username": "Participant3"
}
```


####Create a survey response for participant 1:

`http POST :5000/api/v1.0/survey_responses survey_id=YE3e2Oml7nxK5QGvrLjbWAw4 user_id=NeOdqrlb9YgeZx3X15o4DLMa`

returns

```
HTTP/1.0 201 CREATED
...
{
    "created_at": "2018-08-06T16:38:17.371056",
    "id_": "YE3e2Oml7nxK5QGvrLjbWAw4",
    "survey_id": "YE3e2Oml7nxK5QGvrLjbWAw4",
    "user_id": "NeOdqrlb9YgeZx3X15o4DLMa"
}
```


####Try to create a second response from same participant.

This throws a `DuplicateResponderError`:

`http POST :5000/api/v1.0/survey_responses survey_id=YE3e2Oml7nxK5QGvrLjbWAw4 user_id=NeOdqrlb9YgeZx3X15o4DLMa`

returns

```
HTTP/1.0 400 BAD REQUEST
...
{
    "error": "Bad Request",
    "message": "This user has already completed this survey.",
    "type": "DuplicateResponderError"
}
```


####Successfully create a second response from a new participant.

`http POST :5000/api/v1.0/survey_responses survey_id=YE3e2Oml7nxK5QGvrLjbWAw4 user_id=JW19n8Nj6rpzGQlq5RAOa4oL`

returns

```
HTTP/1.0 201 CREATED
...
{
    "created_at": "2018-08-06T16:40:54.556046",
    "id_": "NeOdqrlb9YgeZx3X15o4DLMa",
    "survey_id": "YE3e2Oml7nxK5QGvrLjbWAw4",
    "user_id": "JW19n8Nj6rpzGQlq5RAOa4oL"
}
```

####Try to create a third survey response.

The survey only wants two participants so this throws a `SurveyNotRecruitingError`

`http POST :5000/api/v1.0/survey_responses survey_id=YE3e2Oml7nxK5QGvrLjbWAw4 user_id=jqXZEdwvYrQn7gk6V0JP75RG`

returns

```
HTTP/1.0 400 BAD REQUEST
...
{
    "error": "Bad Request",
    "message": "Survey YE3e2Oml7nxK5QGvrLjbWAw4 is not recruiting any more participants.",
    "type": "SurveyNotRecruitingError"
}
```

####Create a survey without limiting available places:

`http POST http://localhost:5000/api/v1.0/surveys user_id=YE3e2Oml7nxK5QGvrLjbWAw4 name=MySurveyUnlimited`

returns

```
HTTP/1.0 201 CREATED
...
{
    "available_places": null,
    "created_at": "2018-08-06T18:14:02.551285",
    "id_": "NeOdqrlb9YgeZx3X15o4DLMa",
    "name": "MySurveyUnlimited",
    "survey_responses": "http://localhost:5000/api/v1.0/surveys/NeOdqrlb9YgeZx3X15o4DLMa/responses",
    "user_id": "YE3e2Oml7nxK5QGvrLjbWAw4"
}
```

####Retrieve all surveys:

`http GET :5000/api/v1.0/surveys`

returns

```
HTTP/1.0 200 OK
...
[
    {
        "available_places": 2,
        "created_at": "2018-08-07T16:59:49.519227",
        "id_": "YE3e2Oml7nxK5QGvrLjbWAw4",
        "name": "MySurvey",
        "survey_responses": "http://localhost:5000/api/v1.0/surveys/YE3e2Oml7nxK5QGvrLjbWAw4/responses",
        "user_id": "YE3e2Oml7nxK5QGvrLjbWAw4"
    },
    {
        "available_places": null,
        "created_at": "2018-08-07T17:05:51.735907",
        "id_": "NeOdqrlb9YgeZx3X15o4DLMa",
        "name": "MySurveyUnlimited",
        "survey_responses": "http://localhost:5000/api/v1.0/surveys/NeOdqrlb9YgeZx3X15o4DLMa/responses",
        "user_id": "YE3e2Oml7nxK5QGvrLjbWAw4"
    }
]
```

####Retrieve all responses to a single survey

`http GET http://localhost:5000/api/v1.0/surveys/YE3e2Oml7nxK5QGvrLjbWAw4/responses`

returns

```
HTTP/1.0 200 OK
...
[
    {
        "created_at": "2018-08-07T17:09:04.073801",
        "id_": "YE3e2Oml7nxK5QGvrLjbWAw4",
        "survey_id": "YE3e2Oml7nxK5QGvrLjbWAw4",
        "user_id": "NeOdqrlb9YgeZx3X15o4DLMa"
    },
    {
        "created_at": "2018-08-07T17:09:11.267327",
        "id_": "NeOdqrlb9YgeZx3X15o4DLMa",
        "survey_id": "YE3e2Oml7nxK5QGvrLjbWAw4",
        "user_id": "JW19n8Nj6rpzGQlq5RAOa4oL"
    }
]
```

####Retrieve a single user resource:

`http GET :5000/api/v1.0/users/YE3e2Oml7nxK5QGvrLjbWAw4`

returns

```
HTTP/1.0 200 OK
...
{
    "id_": "YE3e2Oml7nxK5QGvrLjbWAw4",
    "surveys": "http://localhost:5000/api/v1.0/users/YE3e2Oml7nxK5QGvrLjbWAw4/surveys",
    "username": "Username"
}
```


####Retrieve all surveys belonging to a user

`http GET http://localhost:5000/api/v1.0/users/YE3e2Oml7nxK5QGvrLjbWAw4/surveys`

returns

```
HTTP/1.0 200 OK
...
[
    {
        "available_places": 2,
        "created_at": "2018-08-07T16:59:49.519227",
        "id_": "YE3e2Oml7nxK5QGvrLjbWAw4",
        "name": "MySurvey",
        "survey_responses": "http://localhost:5000/api/v1.0/surveys/YE3e2Oml7nxK5QGvrLjbWAw4/responses",
        "user_id": "YE3e2Oml7nxK5QGvrLjbWAw4"
    },
    {
        "available_places": null,
        "created_at": "2018-08-07T17:05:51.735907",
        "id_": "NeOdqrlb9YgeZx3X15o4DLMa",
        "name": "MySurveyUnlimited",
        "survey_responses": "http://localhost:5000/api/v1.0/surveys/NeOdqrlb9YgeZx3X15o4DLMa/responses",
        "user_id": "YE3e2Oml7nxK5QGvrLjbWAw4"
    }
]
```



##Design trade-offs and decisions
- To match the `id_` format provided in the challenge description I have used hashing. This has the added
benefit of obscuring actual resource IDs from third parties.
- Because of the time limit, I have not implemeneted:
    - authorization for API calls
    - pagination when returning collections of resources (e.g. when returning all responses belonging to a survey, or all surveys belonging to a user)
    - I have not provided docstrings for all methods.
- When checking whether a survey is still recruiting, I perform a database query to count the number of survey responses.
A faster method would be for the survey database entry to keep an integer count of `places_remaining` which could be decreased each time a
new survey response is added.
- I have attempted to match the examples provided by Prolific when designing my resource responses. However, in production I would consider
using links when referencing other resources (e.g. the user that created a survey) instead of the resource id. I would also include more meta information.
- I set `FLASK_ENV=development` in the `.flaskenv` file so you will see a traceback should anything not work. In production this should be set to `production`.


