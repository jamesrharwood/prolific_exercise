from app import create_app, db
from app.models import User, Survey, SurveyResponse

app = create_app()

@app.shell_context_processor
def make_sell_context():
    return {'db':db, 'User':User, 'Survey':Survey, 'SurveyResponse':SurveyResponse}
